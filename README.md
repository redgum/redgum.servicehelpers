# Redgum service helpers

INTERNAL NOTE:
Do not store any passwords or connection strings in this project, this is a public repo

This is a "meta library" providing functionality to help creating services. Currently this only works with .net core 1.1

TODO more instructions

## dotnet core 2.0 support
Master branch targets dotnet core 2.2. If you need 2.0 support, there's a core2.0 branch. If you're making changes, try and keep feature compatibility between the branches where possible

## To add all services to MVC Core

In ConfigureServices(IServiceCollection services)

```services.Configure<ServiceHelperOptions>(options => Configuration.GetSection("ServiceHelper").Bind(options));```

In Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)

```app.UseServiceHelpers();```

 (add early in the chain as you need the security stuff to happen)

 ## Code gen

Visit 

```[website]/csharp```

to get c# client code

```[website]/typescript```

for typescript client code

```[website]/swagger```

for raw swagger docs

```[website]/serverInfo```

to see some information about the current environment (more to come)

## Security

This middleware provides a super simple check for a secret in the `ServiceHelper-AuthKey` header.

This should be in your config 

```
  "ServiceHelper": {
    "AuthKey": "abc123",
  }

```

## Individual middleware

Each bit can be used by itself

```
.UseSwaggerUi(Assembly.GetEntryAssembly(), new SwaggerUiSettings()
{
    DocumentProcessors =
    {
        new SecurityDefinitionAppender("apikey", new SwaggerSecurityScheme
        {
            Type = SwaggerSecuritySchemeType.ApiKey,
            Name = SecretAuthMiddleware.AuthKey,
            In = SwaggerSecurityApiKeyLocation.Header
        })
    }
})
.UseSecretAuth()
.UseClientGen();
```