﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Redgum.ServiceHelpers
{
    public class APIBaseController : Controller
    {

        protected APIResponseModel Success(string message = null) { return new APIResponseModel() { Success = true, Message = message }; }
        protected APIResponseModel<T> Success<T>(T data, string message = null) { return new APIResponseModel<T>() { Data = data, Success = true, Message = message }; }
        protected APIResponseModel Error(string message = null) { return new APIResponseModel() { Success = false, Message = message }; }
        protected APIResponseModel<T> Error<T>(string message = null) { return new APIResponseModel<T>() { Success = false, Message = message }; }

        protected APIResponseModel SuccessOrError(bool success, string message)
        {
            return success
                ? Success()
                : Error(message);
        }

        protected APIResponseModel<T> SuccessOrError<T>(bool success, T data, string message)
        {
            return success
                ? Success(data, message)
                : Error<T>(message);
        }


        /// <summary>
        /// helpful helper for simple cases
        /// that will run a function and return the appropriate
        /// response in the case of an error
        /// .......
        /// usage example:
        /// <code>
        ///     return await Run(async () =>
        ///     {
        ///         await _manageTenantService.Add(new NewTenantModel()
        ///         {
        ///             Identifier = tenant.Identifier,
        ///             Name = tenant.Name,
        ///         });
        ///    });
        /// </code>
        /// </summary>
        /// <param name="fun"></param>
        /// <returns></returns>
        protected async Task<APIResponseModel> Run(Func<Task> fun)
        {
            try
            {
                await fun();
                return new APIResponseModel()
                {
                    Success = true,
                };
            }
            catch (Exception ex)
            {
                HandleRunException(ex);
                return Error(ex.Message);
            }
        }

        protected async Task<APIResponseModel<T>> Run<T>(Func<Task<T>> fun)
        {
            try
            {
                var result = await fun();
                return new APIResponseModel<T>()
                {
                    Success = true,
                    Data = result,
                };
            }
            catch (Exception ex)
            {
                HandleRunException(ex);
                return Error<T>(ex.Message);
            }
        }

        protected APIResponseModel<T> Run<T>(Func<T> fun)
        {
            try
            {
                var result = fun();
                return new APIResponseModel<T>()
                {
                    Success = true,
                    Data = result,
                };
            }
            catch (Exception ex)
            {
                HandleRunException(ex);
                return Error<T>(ex.Message);
            }
        }
        protected virtual void HandleRunException(Exception ex)
        {
            //do nothing, this is for folks to override so they can use rollbar or raygun or whatevs
        }
    }
}
