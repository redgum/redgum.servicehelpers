﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using NSwag;
using NSwag.CodeGeneration.CSharp;
using NSwag.CodeGeneration.TypeScript;
using NSwag.SwaggerGeneration.WebApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Redgum.ServiceHelpers
{
    public class ClientGenMiddleware
    {
        const string AuthKey = "ServiceHelper-AuthKey";

        private readonly RequestDelegate _next;
        ServiceHelperOptions _options;

        public ClientGenMiddleware(RequestDelegate next, IOptions<ServiceHelperOptions> options)
        {
            _next = next;
            _options = options.Value;
        }

        private IEnumerable<Type> GetControllerTypes()
        {
            return Assembly.GetEntryAssembly()
                            .GetTypes()
                            .Where(t => typeof(ControllerBase).IsAssignableFrom(t))
                            ;
        }

        private async Task<SwaggerDocument> GetSwaggerDoc()
        {
            var controllers = GetControllerTypes();

            var settings = new WebApiToSwaggerGeneratorSettings();
            var generator = new WebApiToSwaggerGenerator(settings);
            return await generator.GenerateForControllersAsync(controllers);

        }

        // TODO finish this.. add namespace etc
        private IEnumerable<string> GetCsharpPartialCode()
        {
            var controllers = GetControllerTypes();

            foreach (var controller in controllers)
            {
                var controllerName = controller.Name.TrimEnd("Controller".ToCharArray());


                var s = $@"public partial class {controllerName}Client" +
"    {" +
$"        partial void PrepareRequest(System.Net.Http.HttpClient client, System.Net.Http.HttpRequestMessage request, string url)" +
"        {" +
$"            request.Headers.Add(\"{AuthKey}\", \"{_options.SecretAuthOptions.AuthKey}\");" +
"        }" +
"    }";

                yield return s;
            }

        }

        public async Task Invoke(HttpContext context)
        {
            if (context.Request.Path.Value == "/typescript")
            {
                var doc = await GetSwaggerDoc();
                var gensettings = new SwaggerToTypeScriptClientGeneratorSettings();
                var tsgenerator = new SwaggerToTypeScriptClientGenerator(doc, gensettings);
                var code = tsgenerator.GenerateFile();

                await context.Response.WriteAsync(code);
                return;
            }
            if (context.Request.Path.Value == "/csharp")
            {
                var nspace = Assembly.GetEntryAssembly().GetName().Name;
                var doc = await GetSwaggerDoc();
                var gensettings = new SwaggerToCSharpClientGeneratorSettings();
                gensettings.CSharpGeneratorSettings.Namespace = nspace;
                var tsgenerator = new SwaggerToCSharpClientGenerator(doc, gensettings);
                var code = tsgenerator.GenerateFile();

                //// TEMP
                //// temporary fix until typescript 2.6.2 is out
                //code = code.Replace("headers: {", "headers: <any>{");

                //var partialCode = GetCsharpPartialCode().Aggregate((a, b) => a + Environment.NewLine + b);
                //var allCode = $"{code} {Environment.NewLine} {Environment.NewLine} {partialCode}";

                //await context.Response.WriteAsync(allCode);
                await context.Response.WriteAsync(code);
                return;
            }
            if (context.Request.Path.Value == "/serverInfo")
            {
                var env = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");

                var message = $"Environment: {env}";

                await context.Response.WriteAsync(message);
                return;
            }



            await _next.Invoke(context);
        }
    }

    public static class ClientGenExtensions
    {
        public static IApplicationBuilder UseClientGen(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<ClientGenMiddleware>();
        }
    }
}
