﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Redgum.ServiceHelpers
{
    public class ServiceHelperOptions
    {
        public SecretAuthOptions SecretAuthOptions { get; set; }
    }

    public class SecretAuthOptions
    {
        public string AuthKey { get; set; }
        [Obsolete()]
        public List<string> IgnorePaths { get; set; }

        private List<string> _IgnoreRegExPatterns;
        public List<string> IgnoreRegExPatterns
        {
            get { return _IgnoreRegExPatterns; }
            set
            {
                _IgnoreRegExPatterns = value;
                if (_IgnoreRegExPatterns == null)
                {
                    _IgnoreRegExs = null;
                }
                else
                {
                    _IgnoreRegExs = _IgnoreRegExPatterns.Select(i => new Regex(pattern: i));
                }
            }
        }

        private IEnumerable<Regex> _IgnoreRegExs;
        public IEnumerable<Regex> IgnoreRegExs
        {
            get
            {
                return _IgnoreRegExs;
            }
        }
    }
}
