﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Redgum.ServiceHelpers
{
    public class SecretAuthMiddleware
    {
        public const string AuthKey = "ServiceHelper-AuthKey";

        private readonly RequestDelegate _next;
        ServiceHelperOptions _options;

        public SecretAuthMiddleware(RequestDelegate next, IOptions<ServiceHelperOptions> options)
        {
            _next = next;
            _options = options.Value;
        }
        public async Task Invoke(HttpContext context)
        {

            //var val = IConfiguration.GetValue<string>("Auth:MasterKey");
            //test for auth header, if they have it, let em through, quick and easy
            if (context.Request.Headers[AuthKey] != _options.SecretAuthOptions.AuthKey)
            {
                //if they don't have it, take a bit more time to check if the url is in the ignore list
                //these calls shouldn't be quite as time critical, so and extra clock-cycle or two to check for existence in a list shouldn't kill us
                var path = context.Request.Path.Value;
                var hasRegExMatch = _options.SecretAuthOptions.IgnoreRegExs != null && _options.SecretAuthOptions.IgnoreRegExs.Any(i => i.IsMatch(path));
                if (!hasRegExMatch)
                {
                    //maintain old behaviour for now, delete the checks involving SecretAuthOptions soon and just return the 401
                    path = path.TrimEnd('/');
                    if (_options.SecretAuthOptions.IgnorePaths == null || !_options.SecretAuthOptions.IgnorePaths.Contains(path, StringComparer.OrdinalIgnoreCase))
                    {
                        context.Response.StatusCode = 401; //UnAuthorized
                        return;
                    }
                }
            }

            await _next.Invoke(context);
        }
    }

    public static class SecretAuthExtensions
    {
        public static IApplicationBuilder UseSecretAuth(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<SecretAuthMiddleware>();
        }
    }
}
