﻿//using System;
//using System.Collections.Generic;
//using System.Text;

//namespace Redgum.ServiceHelpers
//{
//    public class ExceptionFilterMiddleware
//    {
//        const string AuthKey = "ServiceHelper-AuthKey";

//        private readonly RequestDelegate _next;
//        ServiceHelperOptions _options;

//        public ExceptionFilterMiddleware(RequestDelegate next, IOptions<ServiceHelperOptions> options)
//        {
//            _next = next;
//            _options = options.Value;
//        }
//        public async Task Invoke(HttpContext context)
//        {

//            //var val = IConfiguration.GetValue<string>("Auth:MasterKey");
//            if (context.Request.Headers[AuthKey] != _options.AuthKey)
//            {
//                context.Response.StatusCode = 401; //UnAuthorized
//                return;
//            }

//            await _next.Invoke(context);
//        }
//    }

//    public static class ExceptionFilterMiddlewareExtensions
//    {
//        public static IApplicationBuilder UseExceptionFilter(this IApplicationBuilder builder)
//        {
//            //return builder.UseMiddleware<ExceptionFilterMiddleware>();

//            builder.UseExceptionHandler(errorApp =>
//            {
//                errorApp.Run(async context =>
//                {
//                    context.Response.StatusCode = 500; // or another Status accordingly to Exception Type
//                    context.Response.ContentType = "application/json";

//                    var error = context.Features.Get<IExceptionHandlerFeature>();
//                    if (error != null)
//                    {
//                        var ex = error.Error;

//                        await context.Response.WriteAsync(new ErrorDto()
//                        {
//                            Code = < your custom code based on Exception Type >,
//                            Message = ex.Message // or your custom message
//                            // other custom data
//                        }.ToString(), Encoding.UTF8);
//                    }
//                });
//            });

//        }
//    }
//}
