﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using NSwag;
using NSwag.AspNetCore;
using NSwag.SwaggerGeneration.Processors.Security;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace Redgum.ServiceHelpers
{
    //this is not middleware, its an extension and it should be named accordingly
    public static class ServiceHelperMiddleware
    {
        public static IApplicationBuilder UseServiceHelpers(this IApplicationBuilder builder)
        {
            return builder
                // do this first so we don't need to send auth stuff
                //[Obsolete("Use AddSwaggerDocument(), UseSwagger() and UseSwaggerUi3() instead.")]
                //.UseSwaggerUi(Assembly.GetEntryAssembly(), new SwaggerUiSettings()
                //{
                //    DocumentProcessors =
                //    {
                //        new SecurityDefinitionAppender("apikey", new SwaggerSecurityScheme
                //        {
                //            Type = SwaggerSecuritySchemeType.ApiKey,
                //            Name = SecretAuthMiddleware.AuthKey,
                //            In = SwaggerSecurityApiKeyLocation.Header
                //        })
                //    }
                //})
                .UseSwagger()
                .UseSwaggerUi3()
                .UseClientGen()
                .UseSecretAuth();
        }

        //this is an attempt at replacing what UseServiceHelpers used to do with the UseSwaggerUi call
        //I don't know if its correct and I don't even know if its a good idea.
        public static IServiceCollection AddSwaggerApiKeyDocumentProcessor(this IServiceCollection services)
        {
           return services.AddSwaggerDocument(document =>
            {
                document.DocumentProcessors.Add(
                    new SecurityDefinitionAppender("apikey", new SwaggerSecurityScheme
                    {
                        Type = SwaggerSecuritySchemeType.ApiKey,
                        Name = "api_key",
                        In = SwaggerSecurityApiKeyLocation.Header
                    })
                );
            });
        }
    }
}
