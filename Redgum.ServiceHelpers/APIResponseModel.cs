﻿using System;

namespace Redgum.ServiceHelpers
{
    public class APIResponseModel
    {
        public bool Success { get; set; }
        public string Message { get; set; }
    }

    public class APIResponseModel<T> : APIResponseModel
    {
        public T Data { get; set; }
    }
}
